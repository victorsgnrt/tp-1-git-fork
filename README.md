# Voraussetzungen

## Windows

[MSYS2](http://www.msys2.org) x86_64 installieren und dann:

```
pacman -S --needed mingw-w64-x86_64-gcc mingw-w64-x86_64-boost mingw-w64-x86_64-openal \
mingw-w64-x86_64-freetype mingw-w64-x86_64-libvorbis mingw-w64-x86_64-libwebp \
mingw-w64-x86_64-dlfcn mingw-w64-x86_64-glew mingw-w64-x86_64-python3 mingw-w64-x86_64-meson \
mingw-w64-x86_64-pkg-config mingw-w64-x86_64-gdb
```

## Arch Linux

```
pacman -S --needed meson gcc sdl2 pkg-config fontconfig libepoxy libwebp openal libvorbis boost
```

# Bauen

```
meson build
ninja -C build
./build/sushi
```
