#include "Gamepad.hpp"
#include "constants.hpp"
#include "engine/Paths.hpp"

#include <jngl.hpp>
#include <jngl/input.hpp>
#include <jngl/debug.hpp>

Gamepad::Gamepad(std::shared_ptr<jngl::Controller> controller, const int playerNr)
: controller(std::move(controller)), playerNr(playerNr) {
}

double Gamepad::getMovement() const {
	if (playerNr == 0) { return -controller->state(jngl::controller::LeftStickY); }
	return controller->state(jngl::controller::LeftStickX);
}

bool Gamepad::getSnake() const {
	return controller->pressed(jngl::controller::A);
}

bool Gamepad::getNeutralSnake() const {
	return controller->pressed(jngl::controller::Y);
}

bool Gamepad::getShoot() const {
	return controller->pressed(jngl::controller::B);
}

bool Gamepad::getSwap() const {
	return controller->pressed(jngl::controller::X);
}

void Gamepad::draw_help(int playerNr) {
	const int x = playerNr == 0 ? -900 : 600;
	const int y = 195;
	jngl::print("Controls ", x, y - 10);
	jngl::draw(GetPaths().Graphics() + "Xbox_button_A", x, y + 32);
	jngl::print("Snake", x + 50, y + 32);
	jngl::draw(GetPaths().Graphics() + "Xbox_button_B", x, y + 74);
	jngl::print("Shoot", x + 50, y + 74);
	jngl::draw(GetPaths().Graphics() + "Xbox_button_X", x, y + 116);
	jngl::print("Swap Side", x + 50, y + 116);
	jngl::draw(GetPaths().Graphics() + "Xbox_button_Y", x, y + 158);
	jngl::print("Gray Snake", x + 50, y + 158);
}
