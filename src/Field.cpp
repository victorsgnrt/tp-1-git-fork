#include "Field.hpp"

#include <jngl.hpp>

Field::Field() {
	borderCell.setType(Cell::Type::BORDER);
}

Cell& Field::getCell(FieldIndex index) {
	if (index.i < 0 || index.i >= FIELD_SIZE || index.j < 0 ||
	    index.j >= FIELD_SIZE) { // Sind wir im Rand?
		return borderCell;
	}
	return cells[index.i][index.j];
}

const Cell& Field::getCell(FieldIndex index) const {
	if (index.i < 0 || index.i >= FIELD_SIZE || index.j < 0 ||
	    index.j >= FIELD_SIZE) { // Sind wir im Rand?
		return borderCell;
	}
	return cells[index.i][index.j];
}

void Field::step() {
	for (auto& row : cells) {
		for (auto& cell : row) {
			cell.step();
		}
	}
}

void Field::draw() const {
	jngl::pushMatrix();
	jngl::translate(-496 + CELL_SIZE / 2, -496 + CELL_SIZE / 2);
	for (auto& row : cells) {
		jngl::pushMatrix();
		for (auto& cell : row) {
			cell.draw();
			jngl::translate(16, 0);
		}
		jngl::popMatrix();
		jngl::translate(0, 16);
	}
	jngl::popMatrix();
}
