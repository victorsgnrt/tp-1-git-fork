#include "FieldIndex.hpp"

#include "constants.hpp"

#include <cmath>

FieldIndex FieldIndex::fromScreenPos(Vector2d pos) {
	FieldIndex rtn;
	// Das lround kann man sich evtl. sparen, aber es funktioniert :P
	rtn.j = std::min(std::max(0, int(std::lround((pos.x + 496.0) / CELL_SIZE - 0.5))), 61);
	rtn.i = std::min(std::max(0, int(std::lround((pos.y + 496.0) / CELL_SIZE - 0.5))), 61);
	return rtn;
}

Vector2d FieldIndex::toScreenPos() const {
	return Vector2d((j - FIELD_SIZE / 2 + 0.5) * CELL_SIZE, (i - FIELD_SIZE / 2 + 0.5) * CELL_SIZE);
}

FieldIndex& FieldIndex::operator+=(const FieldIndex& rhs) {
	i += rhs.i;
	j += rhs.j;
	return *this;
}

bool operator==(const FieldIndex& lhs, const FieldIndex& rhs) {
	return lhs.i == rhs.i && lhs.j == rhs.j;
}

FieldIndex operator+(const FieldIndex& lhs, const FieldIndex& rhs) {
	return FieldIndex{ lhs.i + rhs.i, lhs.j + rhs.j };
}

FieldIndex operator-(const FieldIndex& lhs, const FieldIndex& rhs) {
	return FieldIndex{ lhs.i - rhs.i, lhs.j - rhs.j };
}
