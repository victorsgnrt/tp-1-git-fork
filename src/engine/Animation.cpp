#include "Animation.hpp"

#include "Screen.hpp"
#include "Paths.hpp"
#include "../Game.hpp"

#include <jngl/other.hpp>
#include <fstream>
#include <sstream>
#include <cassert>
#include <iomanip>

Animation::Animation(const std::string& foldername, unsigned int canon, const bool repeat)
: foldername_(foldername + "/"), finished_(false), stepsLeft_(0), repeat(repeat), canon(canon) {
	std::string filename(GetPaths().OriginalGfx() + foldername_ + "frames.txt");
	std::ifstream fin(filename.c_str());
	if (!fin) {
		throw std::runtime_error(std::string("Couldn't open ") + filename);
	}
	while (fin) {
		std::string temp;
		std::getline(fin, temp);
		std::stringstream line(temp);
		std::string command;
		line >> command;
		if (command == "fps") {
			double fps;
			line >> fps;
			const int stepsPerSecond = jngl::getStepsPerSecond();
			stepsPerFrame_ = static_cast<int>(stepsPerSecond / fps);
			stepsLeft_ = stepsPerFrame_;
		}
		if (command == "numberOfFrames") {
			line >> numberOfFrames;
		}
	}
	assert(numberOfFrames > 0 && stepsPerFrame_ > 0 && stepsLeft_ > 0);
}

template <class T> int numDigits(T number) {
	int digits = 0;
	if (number < 0) digits = 1; // remove this line if '-' counts as a digit
	while (number) {
		number /= 10;
		digits++;
	}
	return digits;
}

std::string Animation::CreateFilename(int frameNumber) const {
	std::stringstream filename;
	filename << foldername_ << std::setfill('0') << std::setw(numDigits(numberOfFrames))
	         << frameNumber % numberOfFrames;
	return filename.str();
}

void Animation::draw() const {
	if (!finished_) {
		for (unsigned int i = 0; i < canon; ++i) {
			const unsigned int offset = (i * numberOfFrames / canon);
			if (currentFrame < offset) {
				break;
			}
			GetScreen().drawCentered(CreateFilename(currentFrame + offset),
			                         position);
		}
	}
}

void Animation::setFrame(unsigned int frameNr) {
	currentFrame = frameNr;
}

void Animation::setPosition(Vector2d position) {
	this->position = std::move(position);
}

void Animation::step() {
	--stepsLeft_;
	if (stepsLeft_ <= 0) {
		stepsLeft_ = stepsPerFrame_;
		++currentFrame;
		if (currentFrame >= numberOfFrames) {
			finished_ = !repeat;
			if (finished_) {
				GetGame().remove(this);
			}
		}
	}
}

bool Animation::Finished() {
	return finished_;
}
