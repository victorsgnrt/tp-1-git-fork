#include "helper.hpp"

#include "Vector2d.hpp"

bool pointInsideTriangle(const Vector2d& s, const std::array<Vector2d, 3>& triangle) {
	// based on https://stackoverflow.com/a/9755252
	const Vector2d a2p = s - triangle[0];
	const bool p_ab = (triangle[1].X() - triangle[0].X()) * a2p.Y() -
	                  (triangle[1].Y() - triangle[0].Y()) * a2p.X() > 0;
	if (((triangle[2].X() - triangle[0].X()) * a2p.Y() -
	     (triangle[2].Y() - triangle[0].Y()) * a2p.X() > 0) == p_ab) {
		return false;
	}
	if (((triangle[2].X() - triangle[1].X()) * (s.Y() - triangle[1].Y()) -
	     (triangle[2].Y() - triangle[1].Y()) * (s.X() - triangle[1].X()) > 0) != p_ab) {
		return false;
	}
	return true;
}
