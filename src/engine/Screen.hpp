#pragma once

#include "Singleton.hpp"
#include "Vector2d.hpp"

class Screen : public Singleton<Screen> {
public:
	Screen();
	void draw(const std::string& filename, double x, double y);
	void drawCentered(const std::string& filename, double x, double y);
	void drawCentered(const std::string& filename, const Vector2d& position);
	void drawScaled(const std::string& filename, double x, double y, float xfactor, float yfactor);
	void drawCenteredScaled(const std::string& filename, double x, double y, float factor);
	void drawCenteredScaled(const std::string& filename, double x, double y, float xfactor, float yfactor);
	void drawLine(double xstart, double ystart, double xend, double yend);
	void drawLine(const Vector2d& start, const Vector2d& end);
	void drawScaled(const std::string& filename, double x, double y, float factor);
	int getMouseX() const;
	int getMouseY() const;
	void print(const std::string& text, Vector2d);
	void printCentered(const std::string& text, const Vector2d& position);
	void printCentered(const std::string& text, double x, double y);

	Vector2d getAbsoluteMousePos() const;

private:
	double xCenter;
	double yCenter;
};

Screen& GetScreen();

Vector2d GetWindowVector();
