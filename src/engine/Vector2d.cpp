#include "Vector2d.hpp"

#include <boost/math/constants/constants.hpp>
#include <cmath>
#include <iostream>

bool operator!=(const Vector2d& lhs, const Vector2d& rhs) {
	return lhs.X() != rhs.X() || lhs.Y() != rhs.Y();
}

double operator*(const Vector2d& lhs, const Vector2d& rhs) {
	return lhs.X() * rhs.X() + lhs.Y() * rhs.Y();
}

Vector2d operator*(const Vector2d& lhs, const double v) {
	return Vector2d(lhs.X() * v, lhs.Y() * v);
}

Vector2d operator/(const Vector2d& lhs, const double v) {
	return Vector2d(lhs.X() / v, lhs.Y() / v);
}

Vector2d operator*(const double v, const Vector2d& rhs) {
	return Vector2d(rhs.X() * v, rhs.Y() * v);
}

Vector2d operator/(const double v, const Vector2d& rhs) {
	return Vector2d(rhs.X() / v, rhs.Y() / v);
}

Vector2d operator-(const Vector2d& lhs, const Vector2d& rhs) {
	return Vector2d(lhs.X() - rhs.X(), lhs.Y() - rhs.Y());
}

Vector2d operator+(const Vector2d& lhs, const Vector2d& rhs) {
	return Vector2d(lhs.X() + rhs.X(), lhs.Y() + rhs.Y());
}

Vector2d Vector2d::Normalize() const {
	const double temp = Length();
	return Vector2d(x / temp, y / temp);
}

double Vector2d::toDegree() const {
	return std::atan2(y, x) * 180 / boost::math::constants::pi<double>();
}

double Vector2d::Sum() const {
	return x + y;
}

Vector2d::Vector2d() : x(0), y(0) {
}

Vector2d::Vector2d(double v) : x(v), y(v) {
}

Vector2d::Vector2d(double x, double y) : x(x), y(y) {
}

double Vector2d::LengthSq() const {
	return x * x + y * y;
}

double Vector2d::X() const {
	return x;
}

double Vector2d::Y() const {
	return y;
}

void Vector2d::SetX(const double x) {
	this->x = x;
}

void Vector2d::SetY(const double y) {
	this->y = y;
}

void Vector2d::Set(const double x, const double y) {
	this->x = x;
	this->y = y;
}

Vector2d& Vector2d::operator/=(const double v) {
	x /= v;
	y /= v;
	return *this;
}

Vector2d& Vector2d::operator*=(const double v) {
	x *= v;
	y *= v;
	return *this;
}

Vector2d& Vector2d::operator-=(const Vector2d& rhs) {
	x -= rhs.X();
	y -= rhs.Y();
	return *this;
}

Vector2d& Vector2d::operator+=(const Vector2d& rhs) {
	x += rhs.X();
	y += rhs.Y();
	return *this;
}

double Vector2d::Length() const {
	return sqrt(x * x + y * y);
}

std::ostream& operator<<(std::ostream& out, const Vector2d& vec) {
	out << vec.X() << " " << vec.Y();
	return out;
}
