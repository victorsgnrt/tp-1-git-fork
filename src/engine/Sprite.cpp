#include "Sprite.hpp"

#include "Vector2d.hpp"
#include "Screen.hpp"
#include "Work.hpp"
#include "Paths.hpp"

Sprite::Sprite(Vector2d position) : position(position) {
}

Sprite::Sprite(const std::string& filename, Vector2d position) : position(position) {
	setFilename(filename);
}

Sprite::~Sprite() {
}

void Sprite::add() {
	std::weak_ptr<Work> work = std::dynamic_pointer_cast<Work>(jngl::getWork());
	auto p = work.lock();
	if (p) {
		p->addSprite(this);
	}
	removeFunction = [work](Sprite* s) {
		auto p = work.lock();
		if (p) {
			p->removeSprite(s);
		}
	};
}

void Sprite::remove() {
	removeFunction(this);
}

void Sprite::step() {
}

void Sprite::draw() const {
	jngl::pushMatrix();
	jngl::translate(position.X(), position.Y());
	drawSelf();
	jngl::popMatrix();
}

void Sprite::drawSelf() const {
	sprite->draw();
}

void Sprite::setFilename(const std::string& f) {
	sprite.reset(new jngl::Sprite(GetPaths().Graphics() + f));
}

void Sprite::setPos(Vector2d p) {
	position = p;
}

Vector2d Sprite::getPos() const {
	return position;
}

void Sprite::setRemoveFunction(std::function<void (Sprite *)> f) {
	removeFunction = f;
}
