#pragma once

#include "Vector2d.hpp"

#include <string>
#include <memory>
#include <functional>

class Work;

namespace jngl {
	class Sprite;
}

class Sprite {
public:
	Sprite(Vector2d);
	Sprite(const std::string& filename, Vector2d = Vector2d(0, 0));
	virtual ~Sprite();
	virtual void step();
	virtual void draw() const;
	Vector2d getPos() const;
	virtual void setPos(Vector2d);
	void add();
	void remove();
	void setFilename(const std::string&);
	void setRemoveFunction(std::function<void(Sprite*)>);
protected:
	virtual void drawSelf() const;

	Vector2d position;
private:
	std::shared_ptr<jngl::Sprite> sprite;
	std::function<void(Sprite*)> removeFunction;
};
