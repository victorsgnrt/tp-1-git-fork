#pragma once

#include "engine/Work.hpp"
#include "gui/ButtonBox.hpp"

class PauseMenu : public Work {
public:
	PauseMenu(std::shared_ptr<jngl::Work>);
	~PauseMenu();
	void step() override;
	void draw() const override;
	void Continue();
	void QuitToMenu() const;
private:
	std::shared_ptr<jngl::Work> work_;
	float fadeIn = 0.0f;
	bool fadeOut = false;
};
