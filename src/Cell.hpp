#pragma once

class Cell {
public:
	enum class Type {
		NOTHING,
		PLAYER_A,
		PLAYER_B,
		NEUTRAL,
		BORDER,
	};

	Type getType() const;
	void setType(Type);

	bool isMine(int playerNr) const;

	void step();

	/// Zeichnet die Zelle zentriert an (0, 0)
	void draw() const;

	void setActive(bool, double counterStart);

private:
	Type type = Type::NOTHING;

	/// wenn es auf 0 gesetzt wird, animiert er zur 1
	double animation = 1;

	/// kann auf 1 gesetzt werden um eine Explosion anzuzeigen
	double explosion = 0;

	/// true, wenn durch dieses Feld eine aktive Verbindung besteht
	bool active;

	double counter = 0;
};
