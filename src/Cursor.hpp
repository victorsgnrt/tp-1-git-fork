#pragma once

#include "engine/GameObject.hpp"
#include "FieldIndex.hpp"

#include <boost/optional.hpp>

class Snake;

/// Der Cursor der durch die eigene Schlange fliegt
class Cursor : public GameObject {
public:
	Cursor(FieldIndex fieldPos, FieldIndex direction, int playerNr);

	void step() override;
	void draw() const override;

	/// Gibt Position und Richtung zurück wo was gespawnt werden kann
	std::pair<FieldIndex, FieldIndex> getSpawnPos(boost::optional<int> playerNr) const;

private:
	/// Die Position zu der wir hinanimieren wollen
	Vector2d targetPosition;

	FieldIndex fieldPos;
	FieldIndex direction;
	const FieldIndex initialDirection;
	const int playerNr;
	int counter = 0;
};
