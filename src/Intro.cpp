#include "Intro.hpp"

#include "engine/Fade.hpp"
#include "engine/Screen.hpp"
#include "engine/Paths.hpp"
#include "Game.hpp"

#include <jngl.hpp>
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>
#include <sstream>
#include <thread>

Intro::Intro() : finished_(false) {
	jngl::setFont(GetPaths().data() + "Lato-Regular.ttf");
	boost::filesystem::directory_iterator end;
	for (boost::filesystem::directory_iterator it(jngl::getPrefix() + GetPaths().data() +
	                                              "sfx/");
	     it != end; ++it) {
		std::string file = it->path().generic_string();
		const std::string extension = ".ogg";
		if (file.substr(file.size() - extension.size()) == extension) {
			loadingFunctions.emplace_back([file]() {
				return jngl::load(std::string(file).erase(0, jngl::getPrefix().size()));
			});
		}
	}
}

void Intro::draw() const {
	jngl::setFontSize(130);
	jngl::setFontColor(0, 0, 0);
	GetScreen().printCentered("Snake Line Transmission", 0, -100);
	jngl::setFontSize(80);
	jngl::setFontColor(0, 0, 0, blink > 255 ? 510 - blink : blink);

	jngl::pushMatrix();
	jngl::translate(0, 250);
	float percentage;
	if (resizeGraphics_.isFinished(percentage)) {
		if (currentIndex == loadingFunctions.size()) {
			finished_ = true;
			GetScreen().printCentered("Press any key ...", 0, 0);
		} else {
			GetScreen().printCentered("Loading Sounds ...", 0, 0);
			std::vector<jngl::Finally> loaders;
			for (size_t i = 0; i < std::thread::hardware_concurrency(); ++i) {
				loaders.emplace_back(loadingFunctions[currentIndex]());
				++currentIndex;
				if (currentIndex == loadingFunctions.size()) {
					break;
				}
			}
		}
	} else {
		std::stringstream sstream;
		sstream << "Loading Graphics ...";
		GetScreen().printCentered(sstream.str(), 0, 0);
	}
	jngl::popMatrix();

	jngl::setFontColor(0, 0, 0, 255);
	jngl::setFontSize(70);
	GetScreen().printCentered("v0.1.0", 0, 700);
	GetScreen().printCentered("InnoGames Jam #12", 0, 800);
	GetScreen().printCentered("Copyright 2017 Felix Heitmann, Jan Niklas Hasse", 0, 900);
}

void Intro::step() {
	if (finished_) {
		blink += 5;
	}
	if (blink > 2 * 255) {
		blink = 0;
	}
	if ((jngl::mousePressed() || jngl::keyPressed(jngl::key::Any)) && finished_) {
		jngl::resetFrameLimiter();
		jngl::setWork(std::make_shared<Game>());
	}
}
