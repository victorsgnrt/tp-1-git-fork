#pragma once

#include "FieldIndex.hpp"
#include "engine/GameObject.hpp"
#include "Cell.hpp"

class Snake : public GameObject {
public:
	Snake(FieldIndex pos, FieldIndex direction, Cell::Type);
	void step();
	void draw() const;

private:
	FieldIndex fieldPos;

	/// (1,0), (0,1), (-1,0) oder (0,-1)
	FieldIndex direction;
	const FieldIndex initialDirection;

	Cell::Type cellType;

	int counter = 0;
};
