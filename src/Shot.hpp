#pragma once

#include "FieldIndex.hpp"
#include "engine/GameObject.hpp"

#include <jngl/sprite.hpp>

class Shot : public GameObject {
public:
	Shot(FieldIndex pos, FieldIndex direction, const int playerNr);

	void step();
	void draw() const;
	void destroy(FieldIndex pos);

private:
	/// Die Position zu der wir hinanimieren wollen
	Vector2d targetPosition;

	FieldIndex fieldPos;

	const FieldIndex direction;

	int counter = 0;

	jngl::Sprite sprite;
	jngl::Sprite spriteRed;
	int spriteCounter = 0;
	int spriteRotation = 0;
};
