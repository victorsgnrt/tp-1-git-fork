#pragma once

#include "engine/Work.hpp"
#include "engine/ResizeGraphics.hpp"

#include <jngl/Finally.hpp>

class Intro : public Work {
public:
	Intro();
	void step() override;
	void draw() const override;

private:
	int blink = 255;

	// Diese beiden sind mutable, da wir beim Zeichnen der Frames Laden:
	mutable bool finished_;
	mutable ResizeGraphics resizeGraphics_;
	mutable std::vector<std::function<jngl::Finally()>> loadingFunctions;
	mutable size_t currentIndex = 0;
};
