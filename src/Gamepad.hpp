#pragma once

#include "Control.hpp"

#include <jngl/Controller.hpp>

class Gamepad : public Control {
public:
	Gamepad(std::shared_ptr<jngl::Controller>, int playerNr);
	double getMovement() const override;
	bool getSnake() const override;
	bool getNeutralSnake() const override;
	bool getShoot() const override;
	bool getSwap() const override;
	void draw_help(int playerNr) override;

private:
	std::shared_ptr<jngl::Controller> controller;
	const int playerNr;
};
