#include "Cell.hpp"

#include "constants.hpp"

#include <jngl.hpp>
#include <cmath>

Cell::Type Cell::getType() const {
	return type;
}

void Cell::setType(const Type type) {
	if(this->type==Type::BORDER){
		return;//der Rand muss immer Rand bleiben
	}
	this->type = type;
	animation = 0;
	if (type == Type::NOTHING) {
		explosion = 1;
	}
}

bool Cell::isMine(const int playerNr) const {
	if (type == Type::NEUTRAL) { return true; }
	if (playerNr == 0 && type == Type::PLAYER_A) { return true; }
	if (playerNr == 1 && type == Type::PLAYER_B) { return true; }
	return false;
}

void Cell::step() {
	animation += (1 - animation) * 0.1;
	explosion -= 0.02;
	active = false; // da Player::step nach uns neu nach Verbindungen checkt
}

void Cell::draw() const {
	jngl::Color color = colorPlayerA;
	if (type == Type::PLAYER_B) {
		color = colorPlayerB;
	}
	if (type == Type::NEUTRAL) {
		color = jngl::Color(130, 130, 130);
	}
	if (explosion > 0) {
		jngl::setColor(255, ((int(explosion * 100) % 8) > 4) ? 255 : 0, 0);
		jngl::pushAlpha(explosion * 255);
		jngl::drawEllipse(0, 0, CELL_SIZE - rand() % 8, CELL_SIZE - rand() % 8);
	} else {
		if (type == Type::NOTHING) {
			return;
		}
		jngl::setColor(color);
		jngl::pushAlpha(animation * 255);
		jngl::drawRect(-CELL_SIZE / 2, -CELL_SIZE / 2, CELL_SIZE, CELL_SIZE);
	}
	if (active) {
		jngl::setColor(255, 255, 255);
		jngl::pushAlpha(std::abs(std::sin(counter)) * 255);
		jngl::pushMatrix();
		jngl::rotate(counter * 50);
		const auto size = CELL_SIZE / 2;
		jngl::drawRect(-size / 2, -size / 2, size, size);
		jngl::popMatrix();
		jngl::popAlpha();
	}
	jngl::popAlpha();
}

void Cell::setActive(bool active, const double counter) {
	this->counter = counter;
	this->active = active;
}
